
#include <cstddef>
#include <iostream>

using namespace std;
struct MyVector
{
	int* arr;
	size_t size, capacity;

	void vector_new(int n)
	{
		this->capacity = n;
		this->size = 0;
		this->arr = new int[this->capacity];
	}

	void vector_delete()
	{
		delete[] arr;
		this->capacity = 0;
	}

	void print_vector()
	{
		for (size_t i = 0; i < vector_size(); ++i)
			cout << arr[i] << " ";
		cout << endl;
	}
	
	void vector_push_back(int m)
	{
		if (this->size >= this->capacity)
		{
			this->capacity = capacity * 2;
			int* temp = new int[this->capacity];
			for (int i = 0; i < this->capacity; i++)
				temp[i] = this->arr[i];
			delete[] this->arr;
			this->arr = temp;
		}

		arr[this->size] = m;
		this->size ++;
	}
	
	int& vector_front()
	{
		return arr[0];
	}

	int& vector_back()
	{
		return arr[size - 1];
	}

	int vector_size()
	{
		return size;
	}

	void vector_pop_back()
	{
		this->size--;
	}
};

int main()
{
	MyVector mv;
	int n;
	cout << "Unesi velicinu niza: " << endl;
	cin >> n;
	mv.vector_new(n);

	int m;
	cout << "Unesi element: " << endl;
	while (cin >> m)
		mv.vector_push_back(m);

	cout << "first element " << mv.vector_front() << endl;
	cout << "last element " << mv.vector_back() << endl;
	mv.print_vector();

	cout << "removing last element" << endl;
	mv.vector_pop_back();
	mv.print_vector();

	cout << "size " << mv.vector_size() << endl;
	cout << "capacity " << mv.capacity << endl;

	mv.vector_delete();
}
