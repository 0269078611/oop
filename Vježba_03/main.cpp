#include "OP_vectors.hpp"

int main()
{
	int a = 3;
	int max = 14;
	int min = 5;
	vector<int> v1;
	vector<int> v2;
	//1.
	insert1(v1, a);
	insert2(v2, min, max);
	//2.
	vect3(v1, v2);
	//3.
	vector<int> vs{ 5,7,8,4,3,6 };
	sorting(vs);
	//4.
	vector<int> vr{ 6,8,7,4,5 };
	remove(vr);
	//5.
	string s = "moj moj moj string";
	string sub = "moj";
	substring_count(s, sub);
	//6.
	vector<string> all;
	string s1, s2;
	cout << "Prvi string:" << endl;
	cin >> s1;
	all.push_back(s1);
	cout << "Prvi string:" << endl;
	cin >> s2;
	all.push_back(s2);
	reverse(all);
}