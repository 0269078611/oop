#include "OP_vectors.hpp"
#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
using namespace std;

void print_vector(std::vector<int>& v)
{
	for (int el : v)
		cout << el << " ";
	cout << endl;
}

void print_string(std::vector<string>& v)
{
	for (string el : v)
		cout << el << " ";
	cout << endl;
}
void insert1(vector<int> &v1, int a)
{
	int num;
	for (int i = 0; i < a; i++)
	{
		cout << "Unesite broj:" << endl;
		cin >> num;
		v1.push_back(num);
	}
	print_vector(v1);
	cout << "\nKraj insert1" << endl;
}

void insert2(vector<int> &v2, int min, int max)
{
	int num;
	cout << "Unesite broj:" << endl;
	cin >> num;
	v2.push_back(num);
	while (max > num && min < num)
	{
		cout << "Unesite broj:" << endl;
		cin >> num;
		v2.push_back(num);
	}
	print_vector(v2);
	cout << "\nKraj insert2" << endl;
}

void vect3(vector<int> &v1, vector<int> &v2)
{
	vector<int> v3;
	int counter = 0;
	for (unsigned int i = 0; i < v1.size(); i++)
	{
		for (unsigned int j = 0; j < v2.size(); j++)
		{
			if (v1[i] != v2[j])
			{
				counter++;
			}
		}
		if (counter == v2.size())
		{
			v3.push_back(v1[i]);
		}
		counter = 0;
	}
	cout << "New vector" << endl;
	print_vector(v3);
}

void sorting(vector<int> &vs)
{
	int sum = 0;
	for (unsigned int i = 0; i < vs.size(); i++)
	{

		for (unsigned int j = i + 1; j < vs.size(); j++)
		{
			if (vs[j] < vs[i])
			{
				int temp = vs[i];
				vs[i] = vs[j];
				vs[j] = temp;
			}
		}
		sum += vs[i];
	}
	vs.push_back(sum);
	vs.insert(vs.begin(), 0);
	cout << "Sort" << endl;
	print_vector(vs);
}

void remove(vector<int> &vr)
{
	vector<int>::iterator it;
	int i;
	cin >> i;
	it = vr.begin() + i;
	vr.erase(it);
	cout << "Remove" << endl;
	print_vector(vr);
}

void substring_count(string s, string sub)
{
	int i = 0;
	string::size_type pos = 0;
	while ((pos = s.find(sub, pos)) != std::string::npos)
	{
		++i;
		pos += sub.length();
	}
	cout << "Substring:" << i << endl;
}

void reverse(vector<string> &all)
{
	for (unsigned int i = 0; i < all.size(); i++)
	{
		reverse(all[i].begin(), all[i].end());
	}
	sort(all.begin(), all.end());
	print_string(all);
}

